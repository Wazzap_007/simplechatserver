﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleTCPChatServer
{
	public class ServerInstance
	{
		static TcpListener tcpListener;
		List<ClientInstance> clients = new List<ClientInstance>();

		/// <summary>
		/// Добавляет подключеного пользователя.
		/// </summary>
		/// <param name="ClientInstance"></param>
		protected internal void AddConnection(ClientInstance ClientInstance)
		{
			clients.Add(ClientInstance);
		}

		/// <summary>
		/// Удаляет отключенного пользователя.
		/// </summary>
		/// <param name="id"></param>
		protected internal void RemoveConnection(string id)
		{
			ClientInstance client = clients.FirstOrDefault(c => c.Id == id);
			if (client != null)
				clients.Remove(client);
		}

		/// <summary>
		/// Прослушивает входящие сообщения.
		/// </summary>
		protected internal void Listen()
		{
			try
			{
				tcpListener = new TcpListener(IPAddress.Any, 8888);
				tcpListener.Start();
				Console.WriteLine("Сервер запущен. Ожидание подключений...");

				while (true)
				{
					TcpClient tcpClient = tcpListener.AcceptTcpClient();

					ClientInstance ClientInstance = new ClientInstance(tcpClient, this);
					Thread clientThread = new Thread(new ThreadStart(ClientInstance.Process));
					clientThread.Start();
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				Disconnect();
			}
		}

		/// <summary>
		/// Пересылка сообщения всем подключенным пользователям.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="id"></param>
		protected internal void BroadcastMessage(string message, string id)
		{
			byte[] data = Encoding.Unicode.GetBytes(message);
			for (int i = 0; i < clients.Count; i++)
			{
				// сообщение отправится всем, кроме самого отправителя
				if (clients[i].Id != id)
				{
					clients[i].Stream.Write(data, 0, data.Length);
				}
			}
		}

		/// <summary>
		/// Осановка сервера.
		/// </summary>
		protected internal void Disconnect()
		{
			tcpListener.Stop();

			for (int i = 0; i < clients.Count; i++)
			{
				clients[i].Close();
			}
			Environment.Exit(0);
		}
	}

}
