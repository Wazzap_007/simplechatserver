﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleTCPChatServer
{
	public class Program
	{
		static ServerInstance server;
		static Thread listenThread;
		static void Main(string[] args)
		{
			try
			{
				server = new ServerInstance();
				listenThread = new Thread(new ThreadStart(server.Listen));
				listenThread.Start();
			}
			catch (Exception ex)
			{
				server.Disconnect();
				Console.WriteLine(ex.Message);
				Console.ReadLine();
			}
		}
	}
}
