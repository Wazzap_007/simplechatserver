﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTCPChatServer
{
	public class ClientInstance
	{
		protected internal string Id
		{
			get; private set;
		}
		protected internal NetworkStream Stream
		{
			get; private set;
		}
		string userName;
		TcpClient client;
		ServerInstance server;

		public ClientInstance(TcpClient tcpClient, ServerInstance serverObject)
		{
			Id = Guid.NewGuid().ToString();
			client = tcpClient;
			server = serverObject;
			serverObject.AddConnection(this);
		}

		/// <summary>
		/// Прослушивает сообщения от клиента.
		/// </summary>
		public void Process()
		{
			try
			{
				Stream = client.GetStream();
				string message = GetMessage();
				userName = message;

				message = userName + " вошел в чат";
				server.BroadcastMessage(message, this.Id);
				Console.WriteLine(message);

				// получаем сообщения от клиента, пока тот не отключится
				while (true)
				{
					try
					{
						message = GetMessage();
						message = String.Format("{0}: {1}", userName, message);
						Console.WriteLine(message);
						server.BroadcastMessage(message, this.Id);
					}
					catch
					{
						message = String.Format("{0}: покинул чат", userName);
						Console.WriteLine(message);
						server.BroadcastMessage(message, this.Id);
						break;
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			finally
			{
				server.RemoveConnection(this.Id);
				Close();
			}
		}

		/// <summary>
		/// Обрабатывает полученное сообщение.
		/// </summary>
		/// <returns></returns>
		private string GetMessage()
		{
			byte[] buffer = new byte[64];
			StringBuilder builder = new StringBuilder();
			int bytes = 0;
			do
			{
				bytes = Stream.Read(buffer, 0, buffer.Length);
				builder.Append(Encoding.Unicode.GetString(buffer, 0, bytes));
			}
			while (Stream.DataAvailable);

			string message = builder.ToString();

			// если от пользователя пришло уведомление о выходе, то генерируем исключение для прерывания цикла прослушивания сообщений от него.
			if (String.Equals(message, "ext0"))
				throw new Exception("User " + userName + " has left the chat.");
			return builder.ToString();
		}

		/// <summary>
		/// Закрытие подключения.
		/// </summary>
		protected internal void Close()
		{
			if (Stream != null)
				Stream.Close();
			if (client != null)
				client.Close();
		}
	}
}
